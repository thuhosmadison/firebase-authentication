// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBg-PchrRX0RfSCTFqrz6_e-bhSwGgqU6Y",
  authDomain: "fir-get-token-24925.firebaseapp.com",
  databaseURL: "https://fir-get-token-24925.firebaseio.com",
  projectId: "fir-get-token-24925",
  storageBucket: "fir-get-token-24925.appspot.com",
  messagingSenderId: "420859652037",
  appId: "1:420859652037:web:3e0f61c77f384619028dac",
  measurementId: "G-641QRPY9XJ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

//Global variable
let displayName = "";
let photoURL = "";
let userCurrent = null;
let email = "";
let userToken = null;
let emailVerified = false;
let buttonArray = [
  "btnCreateUser",
  "btnLogout",
  "btnUpdateUser",
  "btnUpdateEmail",
  "btnVerifycationEmail",
  "btnPasswordReset",
  "btnDeleteUser"
];

//Login
$("#submit-login").click(function() {
  let email = $("#email").val();
  let password = $("#password").val();
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(function() {
      window.location.href = "home.html";
    })
    .catch(function(error) {
      let errorCode = error.code;
      let errorMessage = error.message;
      switch (errorCode) {
        case "auth/user-disabled":
          alert(errorMessage);
          break;
        case "auth/user-not-found":
          alert(errorMessage);
          break;
        case "auth/invalid-email":
          alert(errorMessage);
          break;
        case "auth/wrong-password":
          alert(errorMessage);
          break;
        case "auth/operation-not-allowed": //You must enable
          alert(errorMessage);
          break;
        // case "auth/account-exists-with-different-credential": //Email already associated with another account
        //   alert(errorMessage)
        //   break;
        default:
          alert(errorCode);
      }
    });
});

//Logout
function logOut() {
  firebase
    .auth()
    .signOut()
    .then(function() {
      window.location.href = "index.html";
    })
    .catch(function(error) {
      alert(error);
    });
}

$("#btnLogout").click(function() {
  logOut();
});

//User's Status
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    removeAttrDisabled(buttonArray);
    userCurrent = user;
    console.log(user)
    console.log(user.providerData[0].providerId)
    displayName = user.displayName;
    photoURL = user.photoURL;
    email = user.email;
    emailVerified = user.emailVerified;
    if (emailVerified) {
      $("#btnVerifycationEmail").attr("disabled", true);
    } else {
      $("#btnVerifycationEmail").removeAttr("disabled");
    }
    user.getIdToken().then(function(idToken) {
      $("#tokenUser").text(idToken);
    });
    $("#name").text(displayName);
    $("#email").text(email);
    $("#photoUrl").text(photoURL);
    $("#emailVerified").text(emailVerified);
    $("#uid").text(user.uid);
  } else {
    addAttrDisabled(buttonArray);
    displayName = "";
    photoURL = "";
    userCurrent = null;
    email = "";
    $("#loginPage").removeAttr("hidden");
  }
});

//Update USer
$("#btnUpdateUser").click(function() {
  let attrHiddenArray = [
    "inputEmail",
    "inputPassword",
    "name",
    "photoUrl",
    "btnUpdateEmailMain",
    "inputEmailMain",
    "btnCreate"
  ];
  let noAttrHiddenArray = [
    "inputDisplayName",
    "inputPhotoUrl",
    "editCard",
    "btnUpdate",
    "email"
  ];
  addAttrHidden(attrHiddenArray);
  removeAttrHidden(noAttrHiddenArray);
  $("#editCard .my-card").css("justify-content", "space-between");
  $("#inputDisplayName")
    .val(displayName)
    .focus();
  $("#inputPhotoUrl").val(photoURL);
});

$("#btnUpdate").click(function() {
  if (userCurrent) {
    userCurrent
      .updateProfile({
        displayName: $("#inputDisplayName").val(),
        photoURL: $("#inputPhotoUrl").val()
      })
      .then(function() {
        window.alert("Update successfully !");
        displayName = userCurrent.displayName;
        photoURL = userCurrent.photoURL;
        $("#photoUrl").text(photoURL);
        $("#name").text(displayName);
        cancelFunction();
      })
      .catch(function() {
        window.alert("Some thing error !");
      });
  } else {
    alert("You must sign in first");
  }
});

//Create User
$("#btnCreateUser").click(function() {
  let attrHiddenArray = [
    "name",
    "inputDisplayName",
    "photoUrl",
    "inputPhotoUrl",
    "btnUpdate",
    "btnUpdateEmailMain",
    "inputEmailMain"
  ];
  let noAttrHiddenArray = [
    "inputPassword",
    "editCard",
    "btnCreate",
    "email",
    "inputEmail"
  ];
  addAttrHidden(attrHiddenArray);
  removeAttrHidden(noAttrHiddenArray);
  $("#editCard .my-card").css("justify-content", "space-between");
  $("#inputEmail").focus();
});

$("#btnCreate").click(function() {
  if (userCurrent) {
    let email = $("#inputEmail").val();
    let password = $("#inputPassword").val();
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(function(userData) {
        alert(
          `Successfully created user account with uid: ${userData.user.uid}`
        );
        $("#name").text("Name/Email");
        $("#photoUrl").text("Photo Url/Password");
        cancelFunction();
      })
      .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        switch (errorCode) {
          case "auth/email-already-in-use":
            alert(errorMessage);
            break;
          case "auth/weak-password":
            alert(errorMessage);
            break;
          case "auth/invalid-email":
            alert(errorMessage);
            break;
          case "auth/operation-not-allowed":
            alert(errorMessage);
            break;
          default:
            alert(error);
        }
      });
  } else {
    alert("You must sign in first");
  }
});

//Update Email
$("#btnUpdateEmail").click(function() {
  let attrHiddenArray = [
    "inputEmail",
    "inputPassword",
    "inputDisplayName",
    "inputPhotoUrl",
    "btnCreate",
    "btnUpdate",
    "email"
  ];
  let noAttrHiddenArray = [
    "editCard",
    "name",
    "photoUrl",
    "btnUpdateEmailMain",
    "inputEmailMain"
  ];
  addAttrHidden(attrHiddenArray);
  removeAttrHidden(noAttrHiddenArray);
  $("#inputEmailMain")
    .val(userCurrent.email)
    .focus();
  $("#editCard .my-card").css("justify-content", "space-between");
});

$("#btnUpdateEmailMain").click(function() {
  if (userCurrent) {
    let email = $("#inputEmailMain").val();
    userCurrent
      .updateEmail(email)
      .then(function() {
        alert("Update email successfully !");
        email = userCurrent.email;
        $("#email").text(email);
        cancelFunction();
      })
      .catch(function(error) {
        if (error.code === "auth/requires-recent-login") {
          console.log(error.code)
          console.log("Thu", userCurrent)
          userCurrent.reauthenticateWithCredential(firebase.auth.EmailAuthProvider.credential(userCurrent.email, '000000')).then(function() {
            alert("Update email successfully !");
            email = userCurrent.email;
            $("#email").text(email);
            cancelFunction();
          }).catch(function(error) {
            alert(error.message)
          })
        } else {
          alert(error.message)
        }
      });
  } else {
    alert("You must sign in first");
  }
});

//Verification Email
$("#btnVerifycationEmail").click(function() {
  if (userCurrent) {
    if (emailVerified) {
      alert("Your email Verified!");
    } else {
      firebase.auth().languageCode = "vi";
      userCurrent
        .sendEmailVerification()
        .then(function() {
          alert("Email sent");
        })
        .catch(function(error) {
          alert(error);
        });
    }
  } else {
    alert("You must sign in first");
  }
});

//Password reset
$("#btnPasswordReset").click(function() {
  if (userCurrent) {
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(function() {
        alert("Email sent");
        //warnning
        location.reload();
      })
      .catch(function(error) {
        alert(error);
      });
  } else {
    alert("You must sign in first");
  }
});

//Delete User
$("#btnDeleteUser").click(function() {
  if (userCurrent) {
    userCurrent
      .delete()
      .then(function() {
        alert("Deleted user !");
        window.location.href = "index.html";
      })
      .catch(function(error) {
        alert(error);
      });
  } else {
    alert("You must sign in first");
  }
});

//Cancel function
function cancelFunction() {
  let attrHiddenArray = [
    "inputEmail",
    "inputPassword",
    "editCard",
    "inputDisplayName",
    "inputPhotoUrl",
    "inputEmailMain"
  ];
  let noAttrHiddenArray = ["name", "photoUrl", "email"];
  addAttrHidden(attrHiddenArray);
  removeAttrHidden(noAttrHiddenArray);
}

//Button Cancel
$("#btnCancel").click(function() {
  cancelFunction();
});

//Add attribute "hidden"
function addAttrHidden(array) {
  for (i = 0; i < array.length; i++) {
    $(`#${array[i]}`).attr("hidden", true);
  }
}

//Delete attribute "hidden"
function removeAttrHidden(array) {
  for (i = 0; i < array.length; i++) {
    $(`#${array[i]}`).removeAttr("hidden");
  }
}

//Add attribute "disabled"
function addAttrDisabled(array) {
  for (i = 0; i < array.length; i++) {
    $(`#${array[i]}`).attr("disabled", true);
  }
}

//Delete attribute "disabled"
function removeAttrDisabled(array) {
  for (i = 0; i < array.length; i++) {
    $(`#${array[i]}`).removeAttr("disabled");
  }
}

